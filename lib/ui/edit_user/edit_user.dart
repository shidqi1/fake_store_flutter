import 'package:flutter/material.dart';

class EditUserPage extends StatefulWidget {
  const EditUserPage({Key key}) : super(key: key);

  @override
  State<EditUserPage> createState() => _EditUserPageState();
}

class _EditUserPageState extends State<EditUserPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController streetController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(label: Text("Email")),
            controller: emailController,
          ),
          TextField(
            decoration: InputDecoration(label: Text("Username")),
            controller: passwordController,
          ),
          TextField(
            decoration: InputDecoration(label: Text("Password")),
            controller: usernameController,
          ),
          TextField(
            decoration: InputDecoration(label: Text("First Name")),
            controller: firstNameController,
          ),
          TextField(
            decoration: InputDecoration(label: Text("Last Name")),
            controller: lastNameController,
          ),
          TextField(
            decoration: InputDecoration(label: Text("City ")),
            controller: cityController,
          ),
          TextField(
            decoration: InputDecoration(label: Text("Street ")),
            controller: streetController,
          ),
          Container(
            height: 10,
          ),
          ElevatedButton(
              onPressed: () {
                editUser();
              },
              child: Text("Add user"))
        ],
      ),
    ),);
  }

  void editUser() async{

  }
}
