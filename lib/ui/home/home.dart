import 'package:flutter/material.dart';
import 'package:mit_frontend_test/model/UsersResponse.dart';
import 'package:mit_frontend_test/network/network_exception.dart';
import 'package:mit_frontend_test/repository/main_repository.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController controller;
  List<UsersResponse> listOfUser = [];
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    getUsers();
    controller = ScrollController()..addListener(_scrollListener);
  }

  void getUsers() async {
    setState(() {
      isLoading = true;
    });
    try {
      List<UsersResponse> usersResponse = await MainRepository().getUsers();
      setState(() {
        listOfUser = usersResponse;
        isLoading = false;
      });
    } on NetworkException catch (e) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pushNamed(context, "/add");
              },
              icon: Icon(Icons.add))
        ],
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : ListView.builder(
              itemCount: listOfUser.length,
              itemBuilder: (BuildContext context, int index) {
                return listItem(listOfUser[index]);
              }),
    );
  }

  Widget listItem(UsersResponse usersResponse) {
    return ListTile(
      onTap: () {
        Navigator.pushNamed(context, "/detail", arguments: usersResponse.id.toString());
      },
      leading: const Icon(Icons.people),
      title: Text(usersResponse.username),
      subtitle: Text(usersResponse.name.firstname + usersResponse.name.lastname),
    );
  }

  void _scrollListener() {
    print(controller.position.extentAfter);
    if (controller.position.extentAfter < 500) {}
  }
}
