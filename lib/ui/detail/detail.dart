import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mit_frontend_test/model/UsersResponse.dart';
import 'package:mit_frontend_test/network/network_exception.dart';
import 'package:mit_frontend_test/repository/main_repository.dart';

class DetailPage extends StatefulWidget {
  String id;

  DetailPage({Key key, this.id}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  UsersResponse usersResponse;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    getUser();
  }

  void getUser() async {
    setState(() {
      isLoading = true;
    });
    try {
      UsersResponse response = await MainRepository().getUser(widget.id);
      print("userResponseDetail ${usersResponse}");
      setState(() {
        usersResponse = response;
        isLoading = false;
      });
    } on NetworkException catch (e) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("User ${widget.id}"),
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.edit))],
      ),
      body: isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              height: MediaQuery.of(context).size.height * 0.5,
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.person),
                  Text("Username: " + usersResponse.username),
                  Text("Email: " + usersResponse.email),
                  Text("Username: " + usersResponse.username),
                  Text("First Name: " + usersResponse.name.firstname),
                  Text("Last Name: " + usersResponse.name.lastname),
                  Text("City: " + usersResponse.address.city),
                  Text("Street: " + usersResponse.address.street),
                ],
              ),
            ),
    );
  }
}
