import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mit_frontend_test/model/AddUserResponse.dart';
import 'package:mit_frontend_test/network/network_exception.dart';
import 'package:mit_frontend_test/repository/main_repository.dart';

class AddUserPage extends StatefulWidget {
  const AddUserPage({Key key}) : super(key: key);

  @override
  State<AddUserPage> createState() => _AddUserPageState();
}

class _AddUserPageState extends State<AddUserPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: isLoading? Center(child: CircularProgressIndicator(),):Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(label: Text("Email")),
              controller: emailController,
            ),
            TextField(
              decoration: InputDecoration(label: Text("Username")),
              controller: passwordController,
            ),
            TextField(
              decoration: InputDecoration(label: Text("Password")),
              controller: usernameController,
            ),
            TextField(
              decoration: InputDecoration(label: Text("First Name")),
              controller: firstNameController,
            ),
            TextField(
              decoration: InputDecoration(label: Text("Last Name")),
              controller: lastNameController,
            ),
            TextField(
              decoration: InputDecoration(label: Text("City ")),
              controller: cityController,
            ),
            TextField(
              decoration: InputDecoration(label: Text("Street ")),
              controller: streetController,
            ),
            Container(
              height: 10,
            ),
            ElevatedButton(
                onPressed: () {
                  addUser();
                },
                child: Text("Add user"))
          ],
        ),
      ),
    );
  }

  void addUser() async {
    setState(() {
      isLoading = true;
    });
    try {
      Map<String, dynamic> postObject = {
        "email": emailController.text,
        "username": usernameController.text,
        "password": passwordController.text,
      };
      AddUserResponse response = await MainRepository().addUser(postObject);
      if (response != null) {
        Fluttertoast.showToast(msg: "Add User Succes, Id is:${response.id}");
        setState(() {
          isLoading = false;
        });
        Navigator.pop(context);
      }
    } on NetworkException catch (e) {
      setState(() {
        isLoading = false;
      });
    }
  }
}
