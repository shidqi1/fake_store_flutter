import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mit_frontend_test/model/LoginResponse.dart';
import 'package:mit_frontend_test/network/network_exception.dart';
import 'package:mit_frontend_test/repository/main_repository.dart';

import '../../model/UsersResponse.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  void login() async {
    setState(() {
      isLoading = true;
    });
    try {
      LoginResponse response = await MainRepository().login(userNameController.text, passwordController.text);
      if (response.token != null && response.token.isNotEmpty) {
        Fluttertoast.showToast(msg: "Login Success");
        Navigator.pushNamed(context, "/home");
        setState(() {
          isLoading = false;
        });
      }
    } on NetworkException catch (e) {
      Fluttertoast.showToast(msg: "Wrong Username and Password");
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: isLoading ?Center(child: CircularProgressIndicator(),) :Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              decoration: InputDecoration(label: Text("Username")),
              controller: userNameController,
            ),
            TextField(
              decoration: InputDecoration(label: Text("Password")),
              controller: passwordController,
            ),
            ElevatedButton(
                onPressed: () {
                  login();
                },
                child: Text("Login"))
          ],
        ),
      ),
    );
  }
}
