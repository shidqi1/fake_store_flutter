import 'package:mit_frontend_test/model/AddUserResponse.dart';
import 'package:mit_frontend_test/model/UsersResponse.dart';

import '../model/LoginResponse.dart';
import '../network/api_caller.dart';
import '../network/api_service.dart';

class MainRepository {
  final ApiCaller _apiProvider = ApiCaller();

  Future<LoginResponse> login(String username, String password) async {
    LoginResponse response = await ApiService.login(_apiProvider, username, password);
    return response;
  }

  Future<List<UsersResponse>> getUsers() async {
    List<UsersResponse> response = await ApiService.getUsers(
      _apiProvider,
    );
    return response;
  }

  Future<UsersResponse> getUser(String id) async {
    UsersResponse response = await ApiService.getUser(_apiProvider, id);
    return response;
  }

  Future<AddUserResponse> addUser(Map<String, dynamic> object) async {
    AddUserResponse response = await ApiService.addUser(_apiProvider, object);
    return response;
  }


}
