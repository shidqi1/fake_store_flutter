import 'package:flutter/material.dart';
import 'package:mit_frontend_test/ui/add_user/add_user.dart';
import 'package:mit_frontend_test/ui/detail/detail.dart';
import 'package:mit_frontend_test/ui/home/home.dart';
import 'package:mit_frontend_test/ui/login/login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: "/",
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case '/':
            return MaterialPageRoute(builder: (_) => const LoginPage());
          case '/home':
            return MaterialPageRoute(builder: (_) => const HomePage());
          case '/detail':
            return MaterialPageRoute(builder: (_) =>  DetailPage(id: settings.arguments,));
          case '/add':
            return MaterialPageRoute(builder: (_) => const AddUserPage());
          default:
            return _errorRoute();
        }
      },
    );
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Error'),
          centerTitle: true,
        ),
        body: const Center(
          child: Text(
            'Error ! Something went wrong',
            style: TextStyle(color: Colors.red, fontSize: 18.0),
          ),
        ),
      );
    });
  }
}
