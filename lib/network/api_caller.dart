
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import 'network_exception.dart';

class ApiCaller {
  final String _baseUrl = "https://fakestoreapi.com";

  Future<dynamic> get(String url, Map param) async {
    dynamic responseJson;
    try {
      print("$_baseUrl/$url");
      print(param);
      String queryString = "";
      if (param.isNotEmpty) {
        queryString = Uri(queryParameters: param).query;
      }
      // String queryString = Uri(queryParameters: param).query;
      final uri = Uri.parse("$_baseUrl/$url?$queryString");
      // final uri = Uri.parse("$_baseUrl/$url");
      final response = await http.get(uri);
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> post(String url, Object body) async {
    dynamic responseJson;
    try {
      print("$_baseUrl/$url");
      print(body);
      final uri = Uri.parse("$_baseUrl/$url");
      final response = await http.post(uri, body: body);
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> put(String url, Object body) async {
    dynamic responseJson;
    try {
      print("$_baseUrl/$url");
      print(body);
      final uri = Uri.parse("$_baseUrl/$url");
      final response = await http.put(uri, body: body);
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _response(http.Response response) {
    print('response ${response.body.toString()}');
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        throw UnauthorisedException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 404:
        throw NotFoundException(response.body.toString());
      case 422:
        throw UnProcessableEntity(response.body.toString());
      case 499:
        throw NetworkException(response.body.toString());
      case 500 - 609:
        {
          print("serverError");
          throw FetchDataException(response.body.toString());
        }
      default:
        throw FetchDataException('Error occurred while Communication with Server with StatusCode : ${response.statusCode} ${response.body.toString()}');
    }
  }

}