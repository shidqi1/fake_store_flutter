import 'dart:convert';

import 'package:mit_frontend_test/model/AddUserResponse.dart';
import 'package:mit_frontend_test/model/UsersResponse.dart';

import '../model/LoginResponse.dart';
import 'api_caller.dart';

class ApiService {
  static Future<LoginResponse> login(ApiCaller apiProvider, String username, String password) async {
    final response = await apiProvider.post('auth/login', {'username': username, "password": password});
    LoginResponse loginResponse = LoginResponse.fromJson(response);
    return loginResponse;
  }

  static Future<List<UsersResponse>> getUsers(ApiCaller apiProvider) async {
    final response = await apiProvider.get('users', {});
    var dataToList = [...response];
    List<UsersResponse> usersResponse = dataToList.map((e) => UsersResponse.fromJson(e)).toList();
    return usersResponse;
  }
  static Future<UsersResponse> getUser(ApiCaller apiProvider, String id) async {
    final response = await apiProvider.get('users/$id', {});
    print("responseDetail ${response}");
    UsersResponse usersResponse = UsersResponse.fromJson(response);
    return usersResponse;
  }

  static Future<AddUserResponse> addUser(ApiCaller apiProvider, Map<String, dynamic> object) async {
    final response = await apiProvider.post('users', object);
    print("responseDetail ${response}");
    AddUserResponse addUserResponse = AddUserResponse.fromJson(response);
    return addUserResponse;
  }

  static Future<AddUserResponse> editUser(ApiCaller apiProvider, String id,Map<String, dynamic> object) async {
    final response = await apiProvider.post('users/$id', object);
    print("responseDetail ${response}");
    UsersResponse usersResponse = UsersResponse.fromJson(response);
    return usersResponse;
  }
}
