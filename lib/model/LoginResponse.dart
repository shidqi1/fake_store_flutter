/// token : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjIsInVzZXIiOiJtb3JfMjMxNCIsImlhdCI6MTY4MTI5MDI0NH0.P7Gg6w0GqSE9rv3hZwpKIgl034cIMFNL33g5Du86Egc"

class LoginResponse {
  LoginResponse({
      String token,}){
    _token = token;
}

  LoginResponse.fromJson(dynamic json) {
    _token = json['token'];
  }
  String _token;
LoginResponse copyWith({  String token,
}) => LoginResponse(  token: token ?? _token,
);
  String get token => _token;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['token'] = _token;
    return map;
  }

}